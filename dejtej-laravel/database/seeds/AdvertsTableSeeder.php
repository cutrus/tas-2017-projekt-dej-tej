<?php

use App\Advert;
use Illuminate\Database\Seeder;

class AdvertsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Advert::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 50; $i++) {
            Advert::create([
                'title' => $faker->sentence,
                'body' => $faker->paragraph,
                'category' => $faker->randomDigit,
            ]);
        }
    }
}
