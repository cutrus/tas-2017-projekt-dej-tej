<?php
 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Advert;
 
class AdvertsController extends Controller
{
    public function index()
    {
        return Advert::all();
    }

    public function show(Advert $advert)
    {
        return $advert;
    }

    public function store(Request $request)
    {
        $advert = Advert::create($request->all());

        return response()->json($advert, 201);
    }

    public function update(Request $request, Advert $advert)
    {
        $advert->update($request->all());

        return response()->json($advert, 200);
    }

    public function delete(Advert $advert)
    {
        $advert->delete();

        return response()->json(null, 204);
    }
}

