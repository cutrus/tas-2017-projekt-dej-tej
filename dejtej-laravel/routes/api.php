<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'cors'], function(){

   Route::post('login', 'Auth\LoginController@login');
   Route::post('logout', 'Auth\LoginController@logout');
   Route::post('register', 'Auth\RegisterController@register');
});
/*
Route::group(['middleware' => 'auth:api'], function() {
    Route::get('adverts', 'AdvertController@index');
    Route::get('adverts/{advert}', 'AdvertController@show');
    Route::post('adverts', 'AdvertController@store');
    Route::put('adverts/{advert}', 'AdvertController@update');
    Route::delete('adverts/{advert}', 'AdvertController@delete');
});
*/

Route::group(['middleware' => 'cors'], function(){
    Route::get('adverts', 'AdvertsController@index');
    Route::get('adverts/{advert}', 'AdvertsController@show');
});

//Route::group(['middleware' => 'auth:api', 'middleware' => 'cors'], function(){
Route::group(['middleware' => 'auth:api'], function(){
    Route::post('adverts','AdvertsController@store');
    Route::put('adverts/{advert}','AdvertsController@update');
    Route::delete('adverts/{advert}', 'AdvertsController@delete');
});
/*    Route::get('fileentry', 'FileEntryController@index');
    Route::get('fileentry/get/{filename}', [
	'as' => 'getentry', 'uses' => 'FileEntryController@get']);
    Route::post('fileentry/add',[ 
        'as' => 'addentry', 'uses' => 'FileEntryController@add']);*/


/*
Route::get('adverts', 'AdvertsController@index');
Route::get('adverts/{advert}', 'AdvertsController@show');
Route::post('adverts','AdvertsController@store');
Route::put('adverts/{advert}','AdvertsController@update');
Route::delete('adverts/{advert}', 'AdvertsController@delete');
*/
