import { HtmlService } from './../services/html.service';
import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AdvertValidators } from './advert.validators';
import { AdvertsListComponent } from '../adverts-list/adverts-list.component';

@Component({
  selector: 'add-advert-form',
  templateUrl: './add-advert-form.component.html',
  styleUrls: ['./add-advert-form.component.css']
})
export class AddAdvertFormComponent {
  form: FormGroup;
  
  advertTypes = [
    { id: 1, name: 'Elektronika'},
    { id: 2, name: 'Dom i ogród'},
    { id: 3, name: 'Moda'},
    { id: 4, name: 'Muzyka i edukacja'},
    { id: 5, name: 'Zwierzęta'},
    { id: 6, name: 'Inne'}
  ];

  constructor(private service: HtmlService) { }

  createAdvert(advert) {
    console.log(advert);
  }

  ngOnInit() {
    this.form = new FormGroup({
      title: new FormControl('', [
        Validators.required,
        Validators.minLength(3)
      ]),
      advertType: new FormControl('', Validators.required),
      description: new FormControl('', [
        Validators.required,
        Validators.minLength(20)
      ]),
      email: new FormControl('', [
        Validators.required,
        AdvertValidators.emailMustBeCorrect,
        Validators.minLength(8)
      ]),
      phone: new FormControl('', Validators.required),
      pic: new FormControl(),
    });
  }
 
/*  
  createAdvert() {
    this.service.createAdvert(this.advert)
      .subscribe(response => {
        console.log(response.json());
        this.advert['id'] = response.json().id;
        HtmlService.adverts.splice(0, 0, this.advert); 
      })
  }
*/


  submitted = false;

  onSubmit() {
    this.submitted = true;
    console.log(this.submitted);
  }

  get title() {
    return this.form.get('title');
  }
  get advertType() {
    return this.form.get('advertType');
  }
  get description() {
    return this.form.get('description');
  }
  get email() {
    return this.form.get('email');
  }
  get phone() {
    return this.form.get('phone');
  }
}
