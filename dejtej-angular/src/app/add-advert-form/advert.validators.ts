import { AbstractControl, ValidationErrors } from "@angular/forms";

export class AdvertValidators {
    static emailMustBeCorrect(control: AbstractControl) : ValidationErrors | null {
        if(((control.value as string).indexOf('@') == -1) || ((control.value as string).indexOf('.') == -1)) 
            return { emailMustBeCorrect: true };

        return null;    
    }
}