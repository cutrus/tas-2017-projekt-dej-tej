import { Component, OnInit } from '@angular/core';
import { HtmlService } from '../services/html.service';

@Component({
  selector: 'main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {
  adverts: any[];

  constructor(private service: HtmlService) { }

  ngOnInit() {
    this.service.getAdverts()
      .subscribe(adverts => this.adverts = adverts);
  }

  sendAdvertNigga() {
    // update advert
      this.service.updateAdvert(this.adverts[0])
        .subscribe(updatedAdvert => {
          console.log(updatedAdvert);
        })
    
  }
/*
  createAdvert(input: HTMLInputElement) {
    let advert = { title: input.value };
    input.value = '';

    this.service.createAdvert(advert)
      .subscribe(newPost => {
        advert['id'] = newPost.id;
        this.adverts.splice(0, 0, advert);
      })
  }

  updateAdvert(advert) {
    this.service.updateAdvert(advert)
      .subscribe(updatedAdvert => {
        console.log(updatedAdvert);
      })
  }

  deleteAdvert(advert) {
    this.service.deleteAdvert(advert.id)
      .subscribe(
        () => {
          let index = this.adverts.indexOf(advert);
          this.adverts.splice(index, 1);
      })
  } */
}
