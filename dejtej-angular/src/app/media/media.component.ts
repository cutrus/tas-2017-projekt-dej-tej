import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HtmlService } from './../services/html.service';

@Component({
  selector: 'media',
  templateUrl: './media.component.html',
  styleUrls: ['./media.component.css']
})
export class MediaComponent implements OnInit {
  form: FormGroup;
  advert: any;
  adverts: any[];

  constructor(private service: HtmlService) { 
    this.advert = {
      title: '',
      body: '',
      category: "3" 
    };
  }

  signUp() {
    console.log(JSON.stringify(this.advert));

    this.service.createAdvert(this.advert);
  }

  ngOnInit() {
    this.service.getAdverts()
      .subscribe(adverts => this.adverts = adverts);

    this.form = new FormGroup({
      title: new FormControl('', [
        Validators.required,
        Validators.minLength(3)
      ]),
      body: new FormControl('', [
        Validators.required,
        Validators.minLength(3)
      ]),
      category: new FormControl('', [
        Validators.required
      ]),
    });
  }

  submitted = false;

  onSubmit() {
    this.submitted = true;
    console.log(this.submitted);
  }

  get title() {
    return this.form.get('title');
  }
  get body() {
    return this.form.get('body');
  }
  get category() {
    return this.form.get('category');
  }
}
