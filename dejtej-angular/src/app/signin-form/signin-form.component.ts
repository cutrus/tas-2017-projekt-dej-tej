import { AdvertValidators } from './../add-advert-form/advert.validators';
import { HtmlService } from './../services/html.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sign-in-form',
  templateUrl: './signin-form.component.html',
  styleUrls: ['./signin-form.component.css']
})
export class SigninFormComponent {
  form: FormGroup;

  account = {
   email: <string> null,
   password: <string> null
  }
  loggedAccount: any; 
  signIn() {
    this.loggedAccount = this.service.logIn(this.account);
    console.log(this.loggedAccount);
  }

  constructor(private service: HtmlService) { }

  ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.minLength(8)
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(3)
      ]),
    });
  }

  get email() {
    return this.form.get('email');
  }
  get password() {
    return this.form.get('password');
  }
}
