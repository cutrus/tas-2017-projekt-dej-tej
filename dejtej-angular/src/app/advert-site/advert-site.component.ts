import { HtmlService } from './../services/html.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'advert-site',
  templateUrl: './advert-site.component.html',
  styleUrls: ['./advert-site.component.css']
})
export class AdvertSiteComponent {

  adverts: any[];

  constructor(private service: HtmlService) { }

  ngOnInit() {
    this.service.getAdverts()
      .subscribe(adverts => this.adverts = adverts);
  }
}
