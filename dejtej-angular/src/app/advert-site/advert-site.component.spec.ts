import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvertSiteComponent } from './advert-site.component';

describe('AdvertSiteComponent', () => {
  let component: AdvertSiteComponent;
  let fixture: ComponentFixture<AdvertSiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvertSiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertSiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
