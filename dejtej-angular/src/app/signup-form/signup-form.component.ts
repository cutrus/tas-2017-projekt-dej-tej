import { HtmlService } from './../services/html.service';
import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AdvertsListComponent } from '../adverts-list/adverts-list.component';
import { AdvertValidators } from '../add-advert-form/advert.validators';

@Component({
  selector: 'signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.css']
})
export class SignupFormComponent {
  form: FormGroup;
  account = {
   name: <string> null,
   email: <string> null,
   phone: <number> null,
   password: <string> null,
   password_confirmation: <string> null
  }

  constructor(private service: HtmlService) { }

  signUp(account) {
    //console.log(account);
    this.service.createAccount(this.account);
    console.log(JSON.stringify(this.account));
  }

  ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        AdvertValidators.emailMustBeCorrect,
        Validators.minLength(8)
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(3)
      ]),
      password_confirmation: new FormControl('', [
        Validators.required,
        Validators.minLength(3)
      ]),
      phone: new FormControl('', [
        Validators.required,
        Validators.minLength(9)
      ]),
      name: new FormControl('', [
        Validators.required,
        Validators.minLength(3)
      ]),
    });
  }

  submitted = false;

  onSubmit() {
    this.submitted = true;
    console.log(this.submitted);
  }
  
  get name() {
    return this.form.get('name');
  }
  get phone() {
    return this.form.get('phone');
  }
  get email() {
    return this.form.get('email');
  }
  get password() {
    return this.form.get('password');
  }
  get password_confirmation() {
    return this.form.get('password_confirmation');
  }
}
