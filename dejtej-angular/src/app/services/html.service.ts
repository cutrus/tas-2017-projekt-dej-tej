import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

@Injectable()
export class HtmlService {
  private url = 'http://jsonplaceholder.typicode.com/posts';
  private photosUrl = 'http://jsonplaceholder.typicode.com/photos';
  private advertsUrl = 'http://192.168.1.12:8000/api/adverts';
  private registerUrl = 'http://192.168.1.12:8000/api/register';
  private loginUrl = 'http://192.168.1.12:8000/api/login';

  data: any;
    constructor(private http: Http) { }
  
    getPhotosAdverts() {
      return this.http.get(this.photosUrl)
        .map(response => response.json());
    }
    getAdverts() {
      return this.http.get(this.advertsUrl)
        .map(response => response.json());
    }

    createAccount(account) {
      const headers = new Headers({'Content-Type':'application/json'});
      return this.http.post(this.registerUrl, JSON.stringify(account),
      {headers:headers}).subscribe((newData) => console.log(newData));
    }

    logIn(account) {
      const headers = new Headers({'Content-Type':'application/json'});
      return this.http.post(this.loginUrl, JSON.stringify(account),
      {headers:headers}).subscribe(res => {return JSON.stringify});
    }

    createAdvert(advert) {
      const headers = new Headers({'Content-Type':'application/json'});
      return this.http.post(this.advertsUrl, JSON.stringify(advert),
      {headers:headers}).subscribe((newData) => console.log(newData));
    }
  
    updateAdvert(advert) {
      return this.http.patch(this.url + '/' + advert.id, JSON.stringify(advert))
        .map(response => response.json());
    }
  
    deleteAdvert(id) {
      return this.http.delete(this.url + '/' + id)
        .map(response => response.json());
    }
}
