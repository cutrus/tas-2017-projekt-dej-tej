import { HtmlService } from './services/html.service';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { MainPageComponent } from './main-page/main-page.component';
import { HttpModule } from '@angular/http';
import { AddAdvertFormComponent } from './add-advert-form/add-advert-form.component';
import { AdvertsListComponent } from './adverts-list/adverts-list.component';
import { RulesComponent } from './rules/rules.component';
import { AdvertSiteComponent } from './advert-site/advert-site.component';
import { SignupFormComponent } from './signup-form/signup-form.component';
import { SigninFormComponent } from './signin-form/signin-form.component';
import { AboutComponent } from './about/about.component';
import { HelpComponent } from './help/help.component';
import { MediaComponent } from './media/media.component';

const routes = [
  { path: '', component: MainPageComponent },
  { path: 'add-advert', component: AddAdvertFormComponent },
  { path: 'adverts-list', component: AdvertsListComponent },
  { path: 'rules', component: RulesComponent },
  { path: 'advert-site', component: AdvertSiteComponent },
  { path: 'sign-up', component: SignupFormComponent },
  { path: 'sign-in', component: SigninFormComponent },
  { path: 'about', component: AboutComponent },
  { path: 'help', component: HelpComponent },
  { path: 'media', component: MediaComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent,
    AddAdvertFormComponent,
    AdvertsListComponent,
    RulesComponent,
    AdvertSiteComponent,
    SignupFormComponent,
    SigninFormComponent,
    AboutComponent,
    HelpComponent,
    MediaComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule
  ],
  providers: [
    HtmlService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
