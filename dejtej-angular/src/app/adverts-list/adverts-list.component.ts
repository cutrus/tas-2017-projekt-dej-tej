import { Component } from '@angular/core';
import { HtmlService } from '../services/html.service';

@Component({
  selector: 'adverts-list',
  templateUrl: './adverts-list.component.html',
  styleUrls: ['./adverts-list.component.css']
})
export class AdvertsListComponent {
  adverts: any[];

  constructor(private service: HtmlService) { }

  ngOnInit() {
    this.service.getPhotosAdverts()
      .subscribe(adverts => this.adverts = adverts);
  }

  pushAdvert(advert) {
    this.adverts.splice(0, 0, advert);
  }
}
